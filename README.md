# Adonis fullstack application

This is the fullstack boilerplate for AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Session
3. Authentication
4. Web security middleware
5. CORS
6. Edge template engine
7. Lucid ORM
8. Migrations and seeds
9. Mysql
10. Validator
11. Mail

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```

## Env Configuration

This fullstack Adonis JS Blog is using mail confirmation with smtp.

Write the following command below to set up SMTP.

```env
MAIL_CONNECTION=smtp
MAIL_HOST=smtp.example.com //example using mailtrap.io (smtp.mailtrap.io)
MAIL_USERNAME=username
MAIL_PASSWORD=password
```

## Setting Up Database

You can see database example in 

```folder
|- database
   |- migrations
   |- `adoniscrud.sql`
   |- factory.js
```

Export the `adoniscrud.sql` to your mysql database and then set up `.env` file.

```env
DB_CONNECTION=mysql
DB_HOST=127.0.0.1                 //default
DB_PORT=3306                      //default
DB_USER=username                  //default is 'root'
DB_PASSWORD=password              //default is empty
DB_DATABASE=your_database_name
```

## Running The Server

To start the server, you must install nodemon `npm install -g nodemon` 

And then start the server with the following command `nodemon server.js`
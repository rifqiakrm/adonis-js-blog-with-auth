'use strict'

const User = use('App/Models/User')

const { validate } = use('Validator')

const Mail = use('Mail')

const randomString = require('random-string')

class RegisterController {

    async index({ view }){
        return view.render('auth.register')
    }

    async register({request, session, response}){
        const validation = await validate(request.all(), {
            username: 'required|unique:users,username',
            email:'required|email|unique:users,password',
            password: 'required'
        })

        if(validation.fails()){
            session.withErrors(validation.messages()).flashAll()
            return response.redirect('back')
        }

        const user = await User.create({
            username: request.input('username'),
            email: request.input('email'),
            password: request.input('password'),
            confirmation_token: randomString({length:40})
        })

        await Mail.send('auth.emails.confirm_email', user.toJSON(), message => {
            message.to(user.email)
            .from('hello@adonisjs.com')
            .subject('Please confirm your email address')
        })

        session.flash({
            notification:{
                type: 'success',
                message:'Registration successful! A mail has been sent to your email address, please confirm your email address'
            }
        })
        
        return response.redirect('back')
    }

    async confirmEmail({params, session, response}){
        //get user confirmation token
        const user = await User.findBy('confirmation_token', params.token)

        user.confirmation_token = null
        user.is_active = true

        await user.save()

        session.flash({
            notification:{
                type: 'success',
                message:'Your email address has been confirmed!'
            }
        })

        return response.redirect('/login')
    }

}

module.exports = RegisterController

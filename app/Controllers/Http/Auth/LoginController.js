'use strict'

const User = use('App/Models/User')
const Hash = use('Hash')

class LoginController {
    async showLoginForm({auth, view, response}){
        try {
            await auth.check()
            return response.route('home')
        } catch (error) {
            return view.render('auth.login')
        }
    }

    async login({request, auth, session, response}){
        //get form data
        const {email, password, remember} = request.all()


        //retrieve user base on the form data
        const user = await User.query()
            .where('email', email)
            .where('is_active', true)
            .first()

        //verify password
        if(user){
            //login user
            const passwordVerified = await Hash.verify(password, user.password)

            if(passwordVerified){
                await auth.remember(!!remember).login(user)

                return response.route('home')
            }
        }

        session.flash({
            notification:{
                type: 'danger',
                message:`We couldn't verify your password. Make sure you've confrimed your email address`
            }
        })

        return response.redirect('back')
    }
}

module.exports = LoginController

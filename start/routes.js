'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')

Route.on('/').render('home').as('home')

Route.get('/login', 'Auth/LoginController.showLoginForm')

Route.get('/login', 'Auth/LoginController.showLoginForm')

Route.post('/login', 'Auth/LoginController.login').as('login')

Route.get('/register', 'Auth/RegisterController.index')

Route.post('/register', 'Auth/RegisterController.register').as('register')

Route.get('/register/confirm/:token', 'Auth/RegisterController.confirmEmail')

Route.get('/posts', 'PostController.index')

Route.get('/posts/add', 'PostController.add')

Route.get('/posts/edit/:id', 'PostController.edit')

Route.get('/posts/:id', 'PostController.details')

Route.post('/posts', 'PostController.store')

Route.put('/posts/:id', 'PostController.update')

Route.delete('/posts/:id', 'PostController.destroy')

Route.get('/logout', 'Auth/AuthenticatedController.logout')

// Route.get('/test', () => 'Hellow world')

// Route.get('/test2', function(){
//     return 'hellow there';
// })

// Route.get('/test/:id', function({params}){
//     return `This is the id ${params.id}`;
// })

